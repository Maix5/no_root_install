# Install tmux (Terminal MUltipleXes) no Root

This directory contains script which install
[tmux](https://github.com/tmux/tmux) software locally
(on Linux based OS) without reaquirement of ROOT permisions.


## Prerequisites

This script requires following software to be preinstalled:
* wget
* C/C++ (development siute compilers/make/...)


## Script usage

To run this script simply pass as first argument directory path
where this software should be installed.

```bash
./install.sh <path_where_to_install>
```


## Versions

Currentlly this script installs tmux of version `2.8`.
One can change this by changing value of `TMUX_VERSION` variable
(see begining at install.sh script).

```bash
TMUX_VERSION=2.8
```

Note: by changing version of tmux
you may also need to change versions of its dependencies
or add/remome some dependencies.
