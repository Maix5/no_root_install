# No root software isntall scripts

Collection of scripts that can install useful software (on Linux system)
\where user does not have Root access.


## Credits

Project icon: [root cutter](https://thenounproject.com/search/?q=root%20cutter&i=2401029)
by [ProSymbols](https://thenounproject.com/prosymbols/)
from [the Noun Project](http://thenounproject.com/).
