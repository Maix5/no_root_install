#!/bin/bash

# NOTE:
# Infuence for this script came from:
# * http://www.linuxfromscratch.org/blfs/view/8.0/postlfs/sshfs.html
# * http://www.linuxfromscratch.org/blfs/view/8.0/postlfs/fuse.html


# Script for installing sshfs on systems where you don't have root access.
# sshfs will be installed in $INSTALL_DIR/local/bin.
# It's assumed that wget, a C/C++ compiler and Glib are installed.

# exit on error
set -e

SSHFS_VERSION=2.10
INSTALL_DIR=$(realpath $1)

# create our directories
mkdir -p $INSTALL_DIR/local $INSTALL_DIR/sshfs_tmp
cd $INSTALL_DIR/sshfs_tmp

# download source files for tmux, libevent, and ncurses
wget -O sshfs-${SSHFS_VERSION}.tar.gz https://github.com/libfuse/sshfs/releases/download/sshfs-${SSHFS_VERSION}/sshfs-${SSHFS_VERSION}.tar.gz

wget https://github.com/libfuse/libfuse/releases/download/fuse-2.9.9/fuse-2.9.9.tar.gz


# extract files, configure, and compile

###########
# libfuse #
###########
tar xvzf fuse-2.9.9.tar.gz
cd fuse-2.9.9
./configure --prefix=$INSTALL_DIR/local --disable-static
make
# Ignoring this if error occurs,
# because installed exec will be enough to use sshfs.
make install || true
cd ..


#########
# sshfs #
#########
tar xvzf sshfs-${SSHFS_VERSION}.tar.gz
cd sshfs-${SSHFS_VERSION}
export PKG_CONFIG_PATH="$INSTALL_DIR/sshfs_tmp/fuse-2.9.9/"
./configure --prefix=$INSTALL_DIR/local
make
make install
cd ..

# cleanup
rm -rf $INSTALL_DIR/sshfs_tmp

echo "SUCCESS: $INSTALL_DIR/local/bin/sshfs is now available."
echo "To add installed executables to your PATH variable run 'export PATH=\"\$PATH:$INSTALL_DIR/local/bin/\"'."
