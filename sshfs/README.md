# Install sshfs (SSH File System) no Root

This directory contains script which install
[sshfs](https://github.com/libfuse/sshfs) software locally
(on Linux based OS) without reaquirement of ROOT permisions.

This script was influenced by manual build command
presented in these tutorials:

* [Manual sshfs-2.8 instalation](http://www.linuxfromscratch.org/blfs/view/8.0/postlfs/sshfs.html)
* [Manual Fuse-2.9.7 instalation](http://www.linuxfromscratch.org/blfs/view/8.0/postlfs/fuse.html)


## Prerequisites

This script requires following software to be preinstalled:

* wget 
* C/C++ (development siute compilers/make/...)
* Glib
* ssh and sftp (in order to actualy use sshfs)


## Script usage

To run this script simply pass as first argument directory path
where this software should be installed.

```bash
./install.sh <path_where_to_install>
```


## Versions

Currentlly this script installs sshfs of version `2.10`.
One can change this by changing value of `SSHFS_VERSION` variable
(see begining at install.sh script).

```bash
SSHFS_VERSION=2.8
```

Note: by changing version of sshfs
you may also need to change versions of its dependencies
or add/remome some dependencies.
